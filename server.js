// referencia biblioteca express
var express = require('express')
var bodyParser = require('body-parser');
var app = express()
//se mete el fichero de usuarios a una variable
var usuarios = require('./usuarios.json')
var cuentas = require('./cuentas.json')
var comercios = require ('./comercios.json')
var opercome = require ('./opercome.json')
var login = require('./login.json')
//instanciar el servicio FS
// si no va a cambiar nunca se puede meter como constante pero lo hacemos como var
var fs = require('fs')
//incluimos la variable request json
var requestjson = require('request-json')


//quiero que mi aplicacion utilice el bodyParser
app.use(bodyParser.json())

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/dbbancogpi/collections"
var apiKey = "apiKey=xWDBPIQOurVzga5Y4JYUHFxJKmBcfIn4"
var clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)


//hago una peticion a la url de Mlab cuando me llegue una petición a V5
//tengo que redirigir la llamada porque si no tendría que meter la APIKEY en la URL de invocacion
app.get('/apitechu/v5/usuarios', function(req, res){
  clienteMlab.get('', function(err, resM, body){
     if(!err){
       res.send(body)
     }
     else{
       res.send(err)
     }
  })
})

//a partir de un usuario ID de entrada (por URL) recuperamos sus datos
//solo hay que obtener el nombre y apellido


app.get('/apitechu/v5/usuarios/:id', function(req, res){
  var id = req.params.id
  var query = 'q={"id":' + id + '}'
  var filtrado = "&f={'nombre':1, 'apellido':1, '_id':0}"
  clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + filtrado + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body){
     if(!err){
       if (body.length > 0)
//devuelve el primer valor del aray si se han encontrado datos en la query
          res.send(body[0])
       else{
       res.status(404).send('usuario no encontrato')
     }
   }
  })
})

//montar el login a partir de la BBDD de MLAB

app.post('/apitechu/v5/login', function(req, res) {
   var email = req.headers.email
   var password = req.headers.password

   var query = 'q={"email":"' + email + '","password":"' + password + '"}'
   clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
   clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1){
         clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?")
         var cambio = '{"$set":{"logged":true}}'
         clienteMlab.put('?q={"id": '+ body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
             res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
           })
       }
       else {
         res.status(404).send('Usuario no encontrado')
       }
     }
   })
})




//hacemos el logout

app.post('/apitechu/v5/logout', function(req, res) {
var id = req.headers.id

var query = 'q={"id":' + id + ', "logged":true}'
 clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
 clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1)
       {
           clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios")
           var cambio = '{"$set":{"logged":false}}'
           clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
             res.send({"logout":"ok", "id":body[0].id})
           })
       }
       else {
         res.status(200).send('Usuario no logado previamente')
       }
     }
 })
})


// insertamos un nuevo usuario en la BBDD de usuarios en mongo

app.post('/apitechu/v8/usuarios', function(req, res){
 var nuevo = {"id":req.headers.id1,
              "nombre":req.headers.nombre1,
              "apellido":req.headers.apellido1,
              "email":req.headers.email1,
              "password":req.headers.password1
            }
  var id = req.headers.id1
  var email = req.headers.email1
  var query = 'q={"email":"' + email + '"}'

  clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  console.log(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
       if (!err) {
         if (body.length == 1)
         {
               res.status(404).send('Usuario existente')
//               res.send({"identificador no válido  id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
         }
         else {
//           res.status(200).send('Usuario no encontrado')
           console.log("voy por el else")
           clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios")
           clienteMlab.post("?" + apiKey, nuevo, function(errP, resP, bodyP) {
            if (!errP) {
              console.log("parece ok")
              console.log(bodyP)
              res.send("alta efectuada correctamente")
            }
            else{
              console.log("hay error")
              console.log(bodyP)
//              res.send(errP)
              res.status(404).send('Usuario existente')
            }
           })
         }
       }
   })
  })


// insertamos un nuevo movimiento en la BBDD de movimientos en mongo

app.post('/apitechu/v8/movimientos', function(req, res){
 var Iban = req.headers.cuenta
 var fecha = req.headers.fecha
 var importe = req.headers.importe
 var divisa = req.headers.divisa
 var y = 0
 var nuevomov = { "id": y,
                  "fecha":req.headers.fecha,
                  "importe":req.headers.importe,
                  "divisa":req.headers.divisa
                }

 var arraymov = []
 var arraymongo = []
 var query = 'q={"IBAN":' + Iban + '}'
  console.log(Iban)
  console.log(fecha)
  console.log(importe)
  console.log(divisa)
  clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuentas?" + query + "&"+ apiKey)
  console.log(urlMlabRaiz + "/cuentas?" + query + "&"+ apiKey)

  clienteMlab.get('', function(err, resM, body) {
       if (!err) {
         console.log(body[0].IBAN)
         console.log(body[0].movimientos)
         var mov = body[0].movimientos
         console.log(mov)
         console.log("estoy aqui")
         console.log(body[0].saldo)
         console.log(importe)
         if (body[0].saldo < this.importe)
         {
               res.status(404).send('saldo insuficiente en cuenta')
               res.send({"saldo insuficiente  IBAN":body[0].IBAN, "Idcliente":body[0].idcliente, "saldo":body[0].saldo})
         }
         else {
           console.log("hay saldo voy por el else")
           arraymongo.push(body[0].movimientos)
           console.log(arraymongo)
           for (var i = 0; i < mov.length; i++) {
                  console.log("entro en el bucle")
                  arraymov[i] = mov[i]
             }
             console.log(arraymov)
           y = i +1
           console.log("y")
           console.log(y)
           nuevomov.id = y
           arraymov[y] = nuevomov
           console.log(arraymov)
           res.send(arraymov)
         }
       }
         else{
           console.log("hay error")
           console.log(bodyP)
           res.status(404).send('error al recuperar la cuenta')
       }
     })
})




















// A partir de un cliente devolver sus cuentas
app.get('/apitechu/v5/cuentas', function(req, res){
  var idcliente = req.headers.id
  var query = 'q={"idcliente":' + idcliente + '}'
  var filter = 'f={"IBAN":1,"_id":0}'
  clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuentas?" + query  + "&" + filter  + "&"+ apiKey)
  console.log(clienteMlab)

  clienteMlab.get('', function(err, resM, body){
     if(!err){
          console.log(body)
          res.send(body)
        }
   })
 })


 app.get('/apitechu/v5/cuentas/id', function(req, res) {
  var idcliente = req.headers.idcliente

  var query = 'q={"idcliente":' + idcliente + '}'
  var filtro = "&f={'IBAN':1, '_id':0}"
  clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuentas?" + query + filtro + "&l=5&" + apiKey)
  console.log(urlMlabRaiz + "/cuentas?" + query + filtro + "&l=5&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body)
    }
  })
 })


// a partir de un IBAN mostrar sus movimientos

app.get('/apitechu/v5/movimientos', function(req, res){
  var cuenta = req.headers.cuenta
  console.log(cuenta)
  var arraymov = []
  var query = 'q={"IBAN":' + cuenta + '}'
  var filter = 'f={"movimientos":1,"_id":0}'
   clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuentas?" + query  + "&" + filter  + "&"+ apiKey)

   clienteMlab.get('', function(err, resM, body) {
     if(!err){
          console.log("movimientos")
          console.log(body)
          for (var i = 0; i < body.length; i++){
            console.log(body[i].movimientos)
                arraymov.push(body[i].movimientos)
          }
          console.log(arraymov)
          console.log(body)
          res.send(body)
        }
   })
 })


 // a partir de un IBAN mostrar sus movimientos cuando el saldo es mayor que el de entrada

 app.get('/pruebagui/v7/movimientos', function(req, res){
   var cuenta = req.headers.cuenta
   var saldo = req.headers.saldo
   var arraymov = []
    var query = 'q={"IBAN":' + cuenta + '","saldo":"{ $lt:"' + saldo + '"} } ] }"'
   var filter = 'f={"movimientos":1,"_id":0}'
    clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuentas?" + query  + "&" + filter  + "&"+ apiKey)

    clienteMlab.get('', function(err, resM, body) {
      if(!err){

           res.send(body)
         }
    })
  })
























// declaro variable a la que asigno el puerto
var port = process.env.port || 3000

app.listen(port)
console.log("API escuchando en el puerto"  + port)

console.log("hola mundo")


console.log("segunda prueba nodemon")
//request response
app.get('/apitechu/v1', function(req, res){
  //console.log(req)
  res.send({"mensaje":"bienvenido a mi API"})
})
//quiero que me devuelva la lista de usuarios
//hay que meter el json descargado de mockaroo a la carpeta de borradorproyecto
app.get('/apitechu/v1/usuarios', function(req, res){

  res.send(usuarios)
})
console.log ("prueba 0")

app.post('/apitechu/v2/usuarios', function(req, res){
//  var nuevo = {"first_name":req.headers.first_name,
//              "country":req.headers.country}
var nuevo = req.body

//mete en la variable usuarios la variable nuevo que está pillando del header de la petición del postman
  usuarios.push(nuevo)
  console.log(req.headers)

//conviete el json de la variable usuarios a string en la constante datos
  const datos = JSON.stringify(usuarios)
//escribe en el fichero usuarios-2 a partir de la variable datos en formato utf8
  fs.writeFile("./usuarios.json", datos, "utf8", function(err){
  if (err){
    console.log(err)}
  else {
    console.log("fichero guardado")
  }
  })
  res.send("alta OK")
})


//vamos a hacer un delete
//:id identifica que es un parámetro que el usuario mandará
app.delete('/apitechu/v1/usuarios/:id', function(req, res){
//splice empieza a contar a prtir del indice que te paso, y luego selecciono cuantos elementos selecciono
  usuarios.splice(req.params.id-1, 1)
  res.send("usuario borrado")
})
//
console.log ("prueba 1")

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req,res){
console.log("parametros")
console.log (req.params)
console.log("querystring")
console.log (req.query)
console.log("headers")
console.log (req.headers)
console.log("body")
console.log (req.body)
  res.send("prueba monstruo")
})
console.log ("prueba 2")

app.post('/apitechu/v3/login', function(req, res){
var email = req.headers.email
var password = req.headers.password
var idusuario = 0
  for (var i = 0; i < login.length; i++){
  if (email == login[i].email && password == login[i].password)
     {
      idusuario = login[i].id
      login[i].logged = true
      break;
      }
  }
  if (idusuario !=0)
      res.send({"encontrado":"si", "id":idusuario})
  else    {
    res.send({"encontrado":"no"})
  }
})

app.post('/apitechu/v3/logout', function(req, res){
var email = req.headers.email
var idusuario = 0
var usuario_logado = false

  for (var i = 0; i < login.length; i++){
  if (email == login[i].email && login[i].logged == true)
     {
      idusuario = login[i].id
      usuario_logado = true
      login[i].logged = false
      break;
      }
  }
  if (usuario_logado && idusuario !=0)
      res.send({"usuario desconectado  id":idusuario})
  else {
      res.send("usuario no estaba logado o usuario invalido")
}
})

//LISTADO DE CUENTAS
app.get('/apitechu/v1/cuentas', function(req, res){
//   res.send(cuentas)
  var arraylistado = []
  for (var i = 0; i < cuentas.length; i++) {
    arraylistado.push(cuentas[i].IBAN)
  }
  res.send(arraylistado)
})

//a partir de una cuenta listamos sus movimientos
app.get('/apitechu/v2/cuentas', function(req, res){
  var cuenta = req.headers.cuenta
  var arraymov = []
  var id = 0
  console.log(cuenta)

  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].IBAN == cuenta){
      id = cuentas[i].idcliente
      arraymov = cuentas[i].movimientos
      break
    }
  }
  if (id == 0)
    res.send("cuenta sin movimientos o cuenta erronea")
  else { res.send(arraymov)}
})

//a partir de un usuario damos sus cuentas

app.get('/apitechu/v3/cuentas', function(req, res){
  var cliente = req.headers.cliente
  var arraycuentas = []
  var id = 0

  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].idcliente == cliente){
      id = cuentas[i].idcliente
      arraycuentas.push(cuentas[i].IBAN)
    }
  }
  if (id == 0)
    res.send("usuario sin cuentas")
  else { res.send(arraycuentas)}
})

//  NO CLASE a partir de aquí
//Aplicación de comercios con json
//API en la que a partir de un cliente, le devolvemos los comercios que tiene contratad

app.get('/apiguille/v1/comercios', function(req, res){
  var cliente = req.headers.id
  var arraycomercios = []
  var id = 0
  for (var i = 0; i < comercios.length; i++) {

    if (comercios[i].idcliente == cliente){
         id = comercios[i].idcliente
         arraycomercios.push(comercios[i])
    }
  }
  if (id == 0)
    res.send("usuario sin comercios asociados")
  else {res.send(arraycomercios)}
})

//API en la que a partir de un comercio, le devolvemos todos sus movimientos

app.get('/apiguille/v2/comercios', function(req, res){
  var fuc = req.headers.fuc
  var arraymovimientos = []
  var id = 0
  for (var i = 0; i < opercome.length; i++) {

    if (opercome[i].FUC == fuc){
         id = opercome[i].FUC
         arraymovimientos.push(opercome[i])
    }
  }
  if (id == 0)
    res.send("comercio sin movimientos asociados")
  else {res.send(arraymovimientos)}
})

//API en la que a partir de un comercio, y un rango de fechas, le devolvemos todos sus movimientos entre esas fechas

app.get('/apiguille/v3/comercios', function(req, res){
  var fuc = req.headers.fuc
  var fechadesde = req.headers.fechadesde
  var fechahasta = req.headers.fechahasta
  var arraymovimientos = []
  var id = 0
  for (var i = 0; i < opercome.length; i++) {

    if (opercome[i].FUC == fuc && opercome[i].fecha > fechadesde && opercome[i].fecha < fechahasta ){
         id = opercome[i].FUC
         arraymovimientos.push(opercome[i])
    }
  }
  if (id == 0)
    res.send("comercio sin movimientos asociados")
  else {res.send(arraymovimientos)}
})
