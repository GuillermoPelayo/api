# Imagen raíz (la · indica comentario)
FROM node
# Carpeta raíz (parte desde la carpeta donde se está ejecutando)
WORKDIR /apitechu
# Copia de archivos desde donde se está ejecutando a la carpeta apitechu (que cuelga de docker)
ADD . /apitechu
#Instalo los paquetes necesarios
RUN npm install
# Volumen de la imagen. Donde yo guarde el volumen, el contenedor lo podrá ver en el directorio /log
VOLUME ["/logs"]
# decir qué puerto quiero exponer
EXPOSE 3000
# Comando de inicio (es el que tengo que poner cuando ejecute el contenedor)
CMD ["npm", "start"]
