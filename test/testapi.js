//cargamos mocha cahi y http en variables para poder trabajar
var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
//cuando se cargue la aplicación se ejecutará la prueba unitaria definida en testapi.js
var server = require('../server')
// especifica y sirve para validar que lo que esperamos coincide con algún valor establecido en las pruebas
var should = chai.should()
//configurar chai con modulo http
chai.use(chaiHttp)

//definir una test suite

//() =>  significa dado ningún parámetro, hazme esto
describe('Test de conectividad', () => {
//probar que google funciona
  it('google funciona', (done) => {
       chai.request('http://www.google.es')
          .get('/')
          .end((err, res) => {
            //displayamos la respuesta a la llamada
            //console.log(res)
           //para asumir que la prueba es superada debería tener su status valor 200
           res.should.have.status(200)
            done()
          })
  })
})
//definir la segunda prueba
describe('Test de api usuarios', () => {
  it('raiz OK', (done) => {
    chai.request('http://localhost:3000')
       .get('/apitechu/v1')
       .end((err, res) => {
           res.should.have.status(200)
// el mensaje devuelto en el body de la apitechu/v1 debería ser igual a "......"
           res.body.mensaje.should.be.eql("bienvenido a mi API")
         done()
          })
  })
  it('lista usuarios', (done) => {
    chai.request('http://localhost:3000')
       .get('/apitechu/v1/usuarios')
       .end((err, res) => {
           res.should.have.status(200)
// el tipo de dato del body es un array
           res.body.should.be.a('array')
           for (var i = 0; i < res.body.length; i++) {
             res.body[i].should.have.property('first_name')
             res.body[i].should.have.property('country')
           }
         done()
          })
  })
})
